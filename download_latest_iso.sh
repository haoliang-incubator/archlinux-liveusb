#!/usr/bin/env bash

set -e

readonly index_url='http://mirrors.163.com/archlinux/iso/latest'

latest_iso() {
    curl -SsL "$index_url" \
        | ag '(?<=href=")archlinux-\d{4}\.\d{2}\.\d{2}-x86_64\.iso(?=")' --only-matching
}

iso="$(latest_iso)"
if [ -z "$iso" ]; then
    >&2 echo "failed to find latest iso name"
    exit 1
fi

aria2c --continue "$index_url/$iso"
