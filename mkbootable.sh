#!/usr/bin/env bash

readonly drive=${1:?requires first param as drive}
readonly iso=${2:?requires second param as iso file}

echo -n "$drive" | grep -E '^\/dev\/[a-z]+$' 1>/dev/null || {
    >&2 echo "invalid drive param"
    exit 1
}

[ -f "$iso" ] || {
    >&2 echo "iso not exists"
    exit 1
}


[ $UID -ne 0 ] && {
    >&2 echo "needs root privilege to run"
    exit 1
}

info=""
info="$(lsblk $drive -o NAME,SIZE,TYPE,MOUNTPOINT --pairs)" || exit 1

echo "drive info of ${drive}:"
echo
echo $info
echo

# user confirm with some necessary messages: capacity, partitions ...
while read -p "right drive?[yn]" -r answer; do
    case "$answer" in
        y|Y)
            break
            ;;
        n|N)
            >&2 echo "exiting"
            exit 1
            ;;
        *)
            >&2 echo "what did you say?"
            ;;
    esac
done

# ensure not mounted
echo -n $info | grep -E 'MOUNTPOINT="[^"]+"' --only-matching && {
    >&2 echo "some partitions of this drive have been mounted, umount them first"
    exit 1
}

# do the job
dd bs=4M if=${iso} of=${drive} status=progress oflag=sync
